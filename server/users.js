var db = require('./pghelper');

/**
 * Gets the user list
 * @param req
 * @param res
 * @param next
 * @returns {*|ServerResponse}
 */
function getUsers(req, res, next) {
  db.query(
    'SELECT user_name, user_mail, user_type FROM users2 ORDER BY user_id DESC',
    [], 
    function(err, datas){
      if (err){
        console.log(err);
        return res.status(400).send('An error occured 2');
      }
      //console.log(datas);
      return res.send(JSON.stringify(datas));
    }
  );
}

/**
 * Add the user
 * @param req
 * @param res
 * @param next
 * @returns {*|ServerResponse}
 */
function postUser(req, res, next) {
  var user = req.body;
  console.log("user_type=" + user.userType);
  db.query(
    'INSERT INTO users2 (user_name, user_mail, user_type) VALUES ($1, $2, $3) ',
    [user.name, user.mail, user.userType], 
    function(err, datas){
      if (err){
        console.log(err);
        return res.status(400).send('An error occured 2');
      }
      return next();
    }
  );
}

exports.getUsers2 = getUsers;
exports.postUser = postUser;