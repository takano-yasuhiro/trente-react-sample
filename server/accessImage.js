var db = require('./pghelper');

var util = require('util');

/**
 * Add the user
 * @param req
 * @param res
 * @param next
 * @returns {*|ServerResponse}
 */
function uploadImage(req, res, next) {
  console.log("accessImage.js#uploadImage start");
  
  var image = req.body;
  //console.log("accessImage.js#uploadImage param->" + util.inspect(req));
  //console.log("accessImage.js#uploadImage image->" + util.inspect(image));
  db.query(
    'INSERT INTO cc_file (file_name, store_name) VALUES ($1, $2) ',
    ['aa', 'bb'], 
    function(err, datas){
      if (err){
        console.log(err);
        return res.status(400).send('An error occured 2');
      }
      return next();
    }
  );
  console.log("accessImage.js#uploadImage end");
}

exports.uploadImage = uploadImage;