var React = require('react');
var ReactRouter = require('react-router');
var History = ReactRouter.History;
var Link = ReactRouter.Link;

//Definition of header
var Header = React.createClass({
  mixins: [ History ],
  
  handleClick: function(e){
  	/* Log-out processing */
    
    //To login screen
    this.history.push('/');
  },
  render: function(){
    return (
      <header>
        <div style={{position:"absolute", margin: "-15px 0px"}}>
          <h1>--- Header ---</h1>
        </div>
        <div style={{position:"relative", textAlign:"right", paddingTop: "30px"}}>
          <Link to={"/portal"} style={{paddingRight: "5px"}}>Portal</Link>
          <Link to={"/userbox"} style={{paddingRight: "5px"}}>User list</Link>
          <Link to={"/body2"} style={{paddingRight: "5px"}}>Body2</Link>
          <button onClick={this.handleClick}>Sign out</button>
        </div>
        <hr/>
      </header>
    );
  }
});

module.exports = Header;