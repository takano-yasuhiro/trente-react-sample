var React = require('react');
var ReactDOM = require('react-dom');

var UserActions = require('../actions/userActions');
var UserStore = require('../stores/userStore');

var getUserStoreStates = function(){
  return UserStore.getAjaxResult();
};

//That the form and list in one
var UserBox = React.createClass({
  getInitialState:function(){
    return getUserStoreStates();
  },
  componentWillMount:function(){
    UserStore.addLoadListener(this.onViewUsers);
    UserStore.addRegisterListener(this.onUpdatedUser);
  },
  componentWillUnmount:function(){
    UserStore.removeLoadListener(this.onViewUsers);
    UserStore.removeRegisterListener(this.onUpdatedUser);
  },
  onViewUsers:function(){
    this.setState(getUserStoreStates());
  },
  onUpdatedUser:function(){
    //Clear after updating success
    ReactDOM.findDOMNode(this.refs.userform.refs.ipt_id_name).value = "";
    ReactDOM.findDOMNode(this.refs.userform.refs.ipt_id_mail).value = "";
    ReactDOM.findDOMNode(this.refs.userform.refs.ipt_id_user_type).value = "F";
    this.onViewUsers();
  },
  handleAddUser:function(name, mail, userType){
    UserActions.register2({name: name, mail: mail, userType: userType});
  },
  componentDidMount:function(){
    UserActions.load();
  },
  render:function(){
    return(
      <div style={{width:"300px"}}>
        <UserForm addUser={this.handleAddUser} ref="userform"/>
        <hr/>
        <UserList userData={this.state.userData}/>
      </div>
    );
  }
});

//Define the components to display a list one row
var User = React.createClass({
  propTypes:{
    user_name: React.PropTypes.string.isRequired,
    user_mail: React.PropTypes.string,
    user_type: React.PropTypes.string
  },
  handleSendEmail: function(){
    //var userName = ReactDOM.findDOMNode(this.refs.ipt_id_name).value.trim();
    var userMail = ReactDOM.findDOMNode(this.refs.ipt_id_mail).value.trim();
    //var userType = ReactDOM.findDOMNode(this.refs.ipt_id_user_type).value.trim();
    //this.props.addUser(userName, userMail, userType);
    //alert(userMail);
    
    $.get("http://localhost:5000/sendEmail", {to: userMail, subject: 'Service test email', html: "<b>Node.js New world for me</b><br />Email content!"},
	    function(data){
		    if(data == "sent"){
		    	alert("Message sent!");
		    }else{
		    	alert("Can not send!");
		    }
	    }
	);
  },
  render:function(){
    return (
      <tr>
        <td>{this.props.user_name}
        </td>
        <td>{this.props.user_mail}
        	<input type="hidden" ref="ipt_id_mail" value={this.props.user_mail}/>
        </td>
        <td>{this.props.user_type}</td>
        <td>
        	<a href="javascript:void(0);" onClick={this.handleSendEmail}>Send</a>
        </td>
      </tr>
    );
  }
});

//Define the components to display the list itself
var UserList = React.createClass({
  propTypes:{
    userData:React.PropTypes.arrayOf(React.PropTypes.object).isRequired
  },
  handleDownload: function(){
    //alert("Download PDF function.");
    //this.history.push('/download');
    
    window.open('/download');
    
    //$.get("http://localhost:5000/download", {}, function(data){
		//    if(data == "sent"){
		  //  	alert("Message sent!");
		    //}else{
		    	//alert("Can not send!");
		    //}
	    //});
    
    
    //$.get("http://localhost:5000/download", {}, function(data){});
  },
  render:function(){
    var UserNodes = this.props.userData.map(function(user, index){
      return (
        <User user_name={user.user_name} user_mail={user.user_mail} user_type={user.user_type} key={index}/>
      );
    });
	return (
		<div>
			<table>
				<tbody>
					<tr>
						<th>Name</th>
						<th>Mail address</th>
						<th>User type</th>
						<th>Mail</th>
					</tr>
					{UserNodes}
				</tbody>
			</table>
			<hr />
			<div style={{textAlign:"right"}}>
				<button onClick={this.handleDownload}>Download</button>
				{/*<a href="/download" target="_blank" style={{paddingRight: "5px"}}>Download2</a>*/}
			</div>
		</div>
	);
	}
});

//Define the user input form
var UserForm = React.createClass({
  propTypes:{
    addUser:React.PropTypes.func.isRequired
  },
  handleSubmit:function(){
    var userName = ReactDOM.findDOMNode(this.refs.ipt_id_name).value.trim();
    var userMail = ReactDOM.findDOMNode(this.refs.ipt_id_mail).value.trim();
    var userType = ReactDOM.findDOMNode(this.refs.ipt_id_user_type).value.trim();
    this.props.addUser(userName, userMail, userType);
  },
  render:function(){
    return (
      <div>
        <table>
          <tbody>
            <tr>
              <td>
                <label>Name</label>
              </td>
              <td>
                <input type="text" ref="ipt_id_name"/>
              </td>
            </tr>
            <tr>
              <td>
                <label>Mail address</label>
              </td>
              <td>
                <input type="text" ref="ipt_id_mail"/>
              </td>
            </tr>
            <tr>
              <td>
                <label>User type</label>
              </td>
              <td>
                <select ref="ipt_id_user_type">
					<option value="F">Free</option>
					<option value="P">Premium</option>
                </select>
              </td>
            </tr>
          </tbody>
        </table>
        <div style={{textAlign:"right"}}>
          <button onClick={this.handleSubmit}>Add</button>
        </div>
      </div>
    );
  }
});

module.exports = UserBox;