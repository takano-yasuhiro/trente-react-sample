var React = require('react');

//Definition of footer
var Footer = React.createClass({
  render: function(){
    return (
      <footer style={{textAlign: "center"}}>
        <hr/>
        <span> --- Footer --- </span>
      </footer>
    );
  }
});

module.exports = Footer;