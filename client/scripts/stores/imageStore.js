var React = require('react');

var ajax = require('./storeUtils').ajax;

var util = require('util');

var Dispatcher = require('../dispatchers/dispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var imageStore = assign({}, EventEmitter.prototype, {
  data: {userData: []},
  addLoadListener: function (callback) {
    this.on('preview_list', callback);
  },
  removeLoadListener: function (callback) {
    this.removeListener('preview_list', callback);
  },
  addRegisterListener: function (callback) {
    this.on('register', callback);
  },
  removeRegisterListener: function (callback) {
    this.removeListener('register', callback);
  },
  getAjaxResult: function(){
    console.log('imageStore.js#getAjaxResult -> ' + util.inspect(imageStore.data));
    return imageStore.data;
  }
});


imageStore.dispatchToken = Dispatcher.register(function (payload) {
  var registerCallback = function(err, res){
    return callback(err, res, 'upload');
  }
  var previewListCallback = function(err, res){
    return callback(err, res, 'preview_list');
  }
  var callback = function(err, res, name){
    if (err){
      alert(res.text);
      return;
    }
    
    console.log('imageStore.js#callback->' + imageStore.data);
    
    imageStore.data = {userData: JSON.parse(res.text)};
    imageStore.emit(name);
  }.bind(imageStore);

  var actions = {

    preview_list: function (payload) {
      //ajax communicate
      console.log('imageStore.js#actions /preview_list');
      ajax.get("/preview_list", {}, previewListCallback);
    },
    upload: function (payload) {
      //ajax communicate
      console.log('imageStore.js#actions /upload');
      console.log('111111 imageStore.js#actions payload -> ' + util.inspect(payload));
      
      var param = {};
      param.test1 = 'abc';
      param.test2 = 'xyz';
      param.xyz = payload.action.target;
      
      ajax.upload("/upload", param, registerCallback);
      //ajax.upload("/upload", payload.action.target, registerCallback);
              
    //ajax通信する
    // var req = SuperAgent.post('upload');
    // req.attach(payload);
    // req.end(registerCallback);
      //ajax.post("/upload", payload.action.target, registerCallback);
      console.log('333333 imageStore.js#actions');
    }
  };

  actions[payload.action.type] && actions[payload.action.type](payload);
});

module.exports = imageStore;