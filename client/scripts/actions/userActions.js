var Dispatcher = require('../dispatchers/dispatcher');

var userActions = {
  //Acquisition of the user list
  load: function(target){
    Dispatcher.handleServerAction({
      type: 'load',
      target: target
    });
  },
  //User Registration
  register2: function (target) {
    Dispatcher.handleServerAction({
      type: 'register',
      target: target
    });
  }
};

module.exports = userActions;