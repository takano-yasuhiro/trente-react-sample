var Dispatcher = require('../dispatchers/dispatcher');

var util = require('util');

var imageActions = {
  //Acquisition of the user list
  previewList: function(target){
      console.log('ImageActions.js#preview_list');
    Dispatcher.handleServerAction({
      type: 'preview_list',
      target: target
    });
  },
  //User Registration
  upload: function (target) {
      console.log('ImageActions.js#upload');
      console.log('2->' + util.inspect(target));
    Dispatcher.handleServerAction({
      type: 'upload',
      target: target
    });
  }
};

module.exports = imageActions;