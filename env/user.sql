﻿DROP TABLE IF EXISTS users2 CASCADE;
CREATE TABLE users2 (
    user_id            serial primary key,
    user_name          CHARACTER VARYING(30) NOT NULL,
    user_mail          CHARACTER VARYING(30) NOT NULL,
    user_type          CHARACTER VARYING(2)
);

ALTER TABLE users2 add column user_type CHARACTER VARYING(2);



----------------------------------------
-- cc_file
----------------------------------------
DROP TABLE IF EXISTS cc_file CASCADE;
CREATE TABLE cc_file
(
   	file_id      serial primary key,
   	file_name    VARCHAR(99) NOT NULL,
   	store_name   TEXT NOT NULL,
   	store_name_path     TEXT
);
